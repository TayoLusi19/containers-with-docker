# Exercise 0: Clone Git Repository and Use Environment Variables

This exercise outlines the process of cloning an existing Git repository, setting up your own version, and the crucial practice of using environment variables for database configurations within an application.

## Importance of Environment Variables

1. **Security:** Environment variables help avoid hardcoding sensitive information, like database passwords, directly into your application code. This practice prevents exposing secrets in your version control system.
   
2. **Flexibility:** Environment variables allow for the dynamic configuration of application settings across different environments (development, testing, production) without code changes.

## Steps to Clone the Repository and Setup Your Project

### Clone the Repository

1. **Clone the existing repository** to your local machine:

    ```bash
    git clone git@gitlab.com:twn-devops-bootcamp/latest/07-docker/docker-exercises.git
    ```

2. **Navigate to the project directory:**

    ```bash
    cd docker-exercises
    ```

### Remove Original Git History and Initialize New Repository

1. **Remove the `.git` directory:**

    ```bash
    rm -rf .git
    ```

2. **Initialize a new Git repository:**

    ```bash
    git init
    ```

3. **Stage and commit the existing project files:**

    ```bash
    git add .
    git commit -m "Initial commit"
    ```

### Create and Push to Your GitLab Repository

1. **Create a new repository on GitLab:** Visit GitLab, log in, and create a new repository where you will push your project.

2. **Link your local repository to your new GitLab repository:**

    Replace `{gitlab-user}` and `{gitlab-repo}` with your GitLab username and the name of your new repository.

    ```bash
    git remote add origin git@gitlab.com:{gitlab-user}/{gitlab-repo}.git
    ```

3. **Push your changes to GitLab:**

    ```bash
    git push -u origin master
    ```

### Configure Environment Variables

- **Locate Environment Variable Definitions:** The environment variables for database connections are defined in the `src/main/java/com/example/DatabaseConfig.java` file. Review and note the required environment variables.

- **Configure Environment Variables in Deployment:** Set these environment variables in your deployment environment or local development setup to match your database credentials and configurations.

## Best Practices

- **Keep Secrets Out of Version Control:** Never commit sensitive data, such as passwords or API keys, to your Git repository. Use environment variables or secrets management tools instead.
- **Document Required Environment Variables:** Clearly document the environment variables required for your application to run, including their purpose and any default values if applicable.
- **Use `.env` Files for Local Development:** For local development, consider using a `.env` file to easily manage environment variables, and make sure to add `.env` to your `.gitignore` file.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

# Author

Adetayo Michael Ibijemilusi
