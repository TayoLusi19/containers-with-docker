# Exercise 1: Start MySQL Container and Run Java Application

To facilitate local testing of the Java application with a MySQL database, we utilize Docker to start a MySQL container. This approach avoids the need to install MySQL directly on your machine. Follow these steps to get the MySQL container running, set the required environment variables for your application, build the application, and start it to test access from a browser.

## Steps to Start MySQL Container and Run Java Application

### Start MySQL Container Using Docker

1. **Run MySQL container:**
   - Use the official MySQL Docker image to start a container. Set the necessary environment variables for the MySQL instance, such as root password, database name, user, and user password.

    ```bash
    docker run -p 3306:3306 \
    --name mysql \
    -e MYSQL_ROOT_PASSWORD=rootpass \
    -e MYSQL_DATABASE=team-member-projects \
    -e MYSQL_USER=admin \
    -e MYSQL_PASSWORD=adminpass \
    -d mysql:latest \
    mysqld --default-authentication-plugin=mysql_native_password
    ```

   - This command starts a new MySQL container named `mysql` and sets up a database with the specified credentials and database name.

### Set Environment Variables for Java Application

2. **Export environment variables:**
   - Before starting your Java application, set the required environment variables that the application uses to connect to the MySQL database.

    ```bash
    export DB_USER=admin
    export DB_PWD=adminpass
    export DB_SERVER=localhost
    export DB_NAME=team-member-projects
    ```

   - These environment variables (`DB_USER`, `DB_PWD`, `DB_SERVER`, `DB_NAME`) correspond to the credentials and connection information for the MySQL container.

### Build and Start the Java Application

3. **Create the Java JAR file:**
   - Navigate to the root directory of your Java project where the `build.gradle` or `pom.xml` file is located, and use Gradle or Maven to build the project.

    ```bash
    gradle build
    ```

   - This command compiles your Java application and packages it into a JAR file located in the `build/libs` directory.

4. **Start the Java application:**
   - Run the JAR file with `java -jar`, specifying the path to the generated JAR file.

    ```bash
    java -jar build/libs/docker-exercises-project-1.0-SNAPSHOT.jar
    ```

   - Your Java application now starts and connects to the MySQL database running in the Docker container.

### Test the Application

- **Access the application from a browser:** Once the application is running, you can access it using your browser to ensure it's properly connected to the MySQL database and functioning as expected. Make some changes via the application's UI to test the database interaction.

## Best Practices

- **Containerization:** Using Docker for development environments provides a quick and consistent way to deploy applications and dependencies.
- **Environment Variables:** Externalizing configuration through environment variables is key for creating twelve-factor applications, allowing for greater flexibility across different environments.
- **Security:** Ensure sensitive information, especially environment variables for production databases, is securely managed and not exposed in your codebase or build scripts.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

# Author

Adetayo Michael Ibijemilusi
