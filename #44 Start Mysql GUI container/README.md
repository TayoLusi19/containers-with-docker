# Exercise 2: Start phpMyAdmin GUI Container

To facilitate easy management of the MySQL database data through a UI, deploying phpMyAdmin in a Docker container provides a convenient solution. This avoids the need to install phpMyAdmin locally and allows for quick setup and access to database management tools.

## Steps to Deploy phpMyAdmin Using Docker

### Start phpMyAdmin Container

1. **Deploy phpMyAdmin container:**
   - Use the official phpMyAdmin Docker image to start a phpMyAdmin container that connects to your MySQL container. This setup enables phpMyAdmin to manage the MySQL database through a web interface.

    ```bash
    docker run -p 8083:80 \
    --name phpmyadmin \
    --link mysql:db \
    -d phpmyadmin/phpmyadmin
    ```

   - This command maps port 80 in the container to port 8083 on your host machine, allowing you to access phpMyAdmin from a web browser at `localhost:8083`.
   - The `--link mysql:db` option links the phpMyAdmin container to the MySQL container, allowing phpMyAdmin to communicate with the MySQL server.

### Access phpMyAdmin in the Browser

2. **Navigate to phpMyAdmin:**
   - Open your web browser and go to `localhost:8083` to access the phpMyAdmin interface.

### Log in to phpMyAdmin

3. **Use MySQL credentials to log in:**
   - At the phpMyAdmin login page, use either of the following MySQL user credentials to log in and manage your databases:
     - Username: `admin`, Password: `adminpass`
     - Username: `root`, Password: `rootpass`

## Best Practices

- **Container Networking:** For a more robust and scalable solution, consider using Docker networks to facilitate container communication instead of `--link`, which is a legacy feature.
- **Security:** Ensure your phpMyAdmin instance is secured, especially if accessible on a public network. Consider implementing additional security measures such as SSL/TLS encryption and network firewalls.
- **Data Persistence:** For production environments, make sure to use Docker volumes or bind mounts to persist MySQL data beyond the lifecycle of the container.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

# Author

Adetayo Michael Ibijemilusi
