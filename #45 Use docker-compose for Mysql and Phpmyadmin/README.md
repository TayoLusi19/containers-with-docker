# Exercise 3: Use docker-compose for MySQL and phpMyAdmin

To streamline the deployment of services your application requires, such as MySQL and phpMyAdmin, you can utilize Docker Compose to manage these containers simultaneously. This approach not only simplifies the startup process but also enhances the manageability of containerized services and their configurations.

## Creating a docker-compose.yml File

Below is a `docker-compose.yml` file configured to run both MySQL and phpMyAdmin containers, with a volume configured for persistent MySQL database storage.

```yaml
version: '3'
services:
  mysql:
    image: mysql:latest
    ports:
      - "3306:3306"
    environment:
      - MYSQL_ROOT_PASSWORD=rootpass
      - MYSQL_DATABASE=team-member-projects
      - MYSQL_USER=admin
      - MYSQL_PASSWORD=adminpass
    volumes:
      - mysql-data:/var/lib/mysql
    container_name: mysql
    command: --default-authentication-plugin=mysql_native_password
  
  phpmyadmin:
    image: phpmyadmin/phpmyadmin:latest
    environment:
      - PMA_HOST=mysql
    ports:
      - "8083:80"
    container_name: phpmyadmin
    depends_on:
      - mysql

volumes:
  mysql-data:
    driver: local
```

This configuration ensures that both the MySQL and phpMyAdmin services are set up with the necessary environment variables, ports, and volumes for persistent data storage.

## Steps to Start Containers Using Docker Compose

1. **Place the `docker-compose.yml` file** in your project directory where you wish to start the MySQL and phpMyAdmin services.

2. **Start the services** by running the following command in the directory containing your `docker-compose.yml` file:

    ```bash
    docker-compose up
    ```

    This command pulls the necessary images, creates the containers, sets up a volume for MySQL data persistence, and starts the services as configured.

3. **Access phpMyAdmin:** Navigate to `http://localhost:8083` in your web browser to access phpMyAdmin. Log in using the MySQL credentials specified in the docker-compose file.

## Testing Everything Works

- Verify that the MySQL service is accessible and that the `team-member-projects` database exists as expected.
- Use phpMyAdmin to interact with the MySQL database and confirm that data persists across container restarts thanks to the configured volume.

## Best Practices

- **Container Orchestration:** Docker Compose facilitates the management of multi-container Docker applications, making it easier to define and share complex configurations.
- **Security:** Ensure that any sensitive information, such as passwords, is securely managed, preferably using environment variables or secrets management solutions.
- **Volume Management:** Using Docker volumes for persistent data storage is crucial for database containers to ensure data is not lost when containers are stopped or removed.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

# Author

Adetayo Michael Ibijemilusi
