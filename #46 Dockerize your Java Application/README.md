# Exercise 4: Dockerize your Java Application

After successfully testing your application locally with a MySQL database and phpMyAdmin, it's time to deploy it on a server to make it accessible to your team members. To ensure consistency and ease of deployment alongside your database and database UI containers, you will Dockerize your Java application. This allows you to manage all the services using a single Docker Compose file on the server. Here are the steps to Dockerize your Java Application:

## 1. Create a Dockerfile

Create a Dockerfile named `Dockerfile` in the root directory of your Java application project. This Dockerfile specifies the instructions for building a Docker image for your application.

```Dockerfile
FROM openjdk:17.0.2-jdk
EXPOSE 8080
RUN mkdir /opt/app
COPY build/libs/docker-exercises-project-1.0-SNAPSHOT.jar /opt/app
WORKDIR /opt/app
CMD ["java", "-jar", "docker-exercises-project-1.0-SNAPSHOT.jar"]
```

This Dockerfile does the following:

- Uses the openjdk:17.0.2-jdk as the base image.
- Exposes port 8080 to allow external access to your application.
- Creates a directory /opt/app in the container.
- Copies the built JAR file from your project's build/libs directory to /opt/app in the container.
- Sets the working directory to /opt/app.
- Specifies the command to run your Java application.

## 2. Build the Docker Image
Navigate to the directory containing your Dockerfile and run the following command to build the Docker image for your Java application:

```bash
docker build -t java-app:1.0 .
```
- -t java-app:1.0 tags the image with a name (java-app) and a version (1.0). You can choose your own name and version.

## 3. Test the Docker Image
You can test the Docker image locally to ensure it runs correctly before deploying it on the server. Use the following command to run the Docker container:

```bash
docker run -p 8080:8080 java-app:1.0
```

- -p 8080:8080 maps port 8080 from the container to port 8080 on your local machine.

## 4. Access Your Java Application
You can access your Java application in a web browser by navigating to **http://localhost:8080**. Ensure that your application runs as expected within the Docker container.

## 5. Deploy the Docker Image on the Server
To deploy the Docker image on the server, you can either copy the Docker image file (e.g., java-app:1.0) to the server and load it using docker load, or you can push the image to a container registry and pull it on the server. The deployment process will depend on your server's setup and environment.

### Best Practices
-  Dockerize Only What's Necessary: Include in the Docker image only the components and dependencies required to run your application.
-  Environment Configuration: Use environment variables to configure your application at runtime. This allows you to change configurations without modifying the Docker image.
-  Security: Be mindful of security considerations, such as minimizing the attack surface and keeping your Docker image up to date with security patches.

## Contributions
Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

## License
Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

## Author
Adetayo Michael Ibijemilusi
