# Exercise 5: Build and Push Java Application Docker Image

To run your Java application as a Docker image on a remote server, you first need to host it on a Docker repository. In this exercise, you will create a Docker hosted repository on Nexus and then build and push your Java Application Docker Image to this repository.

## 1. Create a Docker Hosted Repository on Nexus

1. Access your Nexus repository manager.
2. Navigate to the administration section.
3. Select "Repositories" or "Repositories & Registries."
4. Create a new Docker Hosted Repository:
   - Give it a name (e.g., `docker-hosted-repo`).
   - Set the Repository Connectors as needed.
   - Configure other repository settings as per your requirements.
5. Save and create the repository.

## 2. Build the Docker Image Locally

Before pushing the Docker image, you need to build it locally using the following steps:

### a. Create a JAR file

Ensure you have the JAR file for your Java application. If you haven't built it yet, you can do so with Gradle:

```bash
gradle build
```

## b. Create the Docker Image
Build the Docker image using the JAR file and tag it with the appropriate repository, image name, and tag:

``` bash
docker build -t {repo-name}/java-app:1.0-SNAPSHOT .
```
Replace {repo-name} with the name of your Docker repository, and customize the image tag as needed.

## 3. Push the Docker Image to the Remote Repository
Now that you have built the Docker image, you can push it to the remote Docker repository hosted on Nexus:

```bash
docker push {repo-name}/java-app:1.0-SNAPSHOT
```
Replace {repo-name} with the name of your Docker repository.

## 4. Verify the Image on Nexus
Once the push is successful, you can verify that the Docker image is available in your Nexus Docker hosted repository. You can view the repository contents from the Nexus web interface.

By completing these steps, your Java Application Docker Image is now hosted on Nexus and can be pulled from the remote server for deployment.

# Contributions
Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License
Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

# Author
Adetayo Michael Ibijemilusi













