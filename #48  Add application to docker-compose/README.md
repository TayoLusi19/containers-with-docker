# Exercise 6: Add Application to Docker Compose

In this exercise, you will add your Java application to a Docker Compose configuration. The goal is to configure all necessary environment variables, making them configurable on the server when deploying the application.

## 1. Create a Docker Compose File

Create a Docker Compose file (e.g., `docker-compose-with-app.yaml`) and configure your Java application container along with the necessary environment variables. Ensure that the environment variables can be overridden externally based on the deployment environment.

```yaml
version: '3'
services:
  my-java-app:
    image: {repo-name}/java-app:1.0-SNAPSHOT # Specify the full image name with the repository name
    environment:
      - DB_USER=${DB_USER}
      - DB_PWD=${DB_PWD}
      - DB_SERVER=${DB_SERVER}
      - DB_NAME=${DB_NAME}
    ports:
      - 8080:8080
    container_name: my-java-app
    depends_on:
      - mysql
  mysql:
    image: mysql
    ports:
      - 3306:3306
    environment:
      - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
      - MYSQL_DATABASE=${DB_NAME}
      - MYSQL_USER=${DB_USER}
      - MYSQL_PASSWORD=${DB_PWD}
    volumes:
      - mysql-data:/var/lib/mysql
    container_name: mysql
    command: --default-authentication-plugin=mysql_native_password
  phpmyadmin:
    image: phpmyadmin
    ports:
      - 8083:80
    environment:
      - PMA_HOST=${PMA_HOST}
      - PMA_PORT=${PMA_PORT}
      - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
    container_name: phpmyadmin
    depends_on:
      - mysql
volumes:
  mysql-data:
    driver: local
```

Replace {repo-name} with the name of your Docker repository and customize other environment variables as needed.

## 2. Set Environment Variables
Before starting the containers, set all the required environment variables for your application:

```bash
export DB_USER=admin
export DB_PWD=adminpass
export DB_SERVER=mysql
export DB_NAME=team-member-projects

export MYSQL_ROOT_PASSWORD=rootpass

export PMA_HOST=mysql
export PMA_PORT=3306
```

## 3. Start Containers
Start all three containers using the Docker Compose file you created:

```bash
docker-compose -f docker-compose-with-app.yaml up
```

By completing these steps, your Java application is now part of the Docker Compose configuration, and all the environment variables are set to be configurable based on the deployment environment.

# Contributions
Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License
Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

# Author
Adetayo Michael Ibijemilusi
