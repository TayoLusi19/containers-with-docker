# Exercise 7: Run Application on Server with Docker Compose

In this exercise, you will deploy your application on a remote server using Docker Compose. You will also address some configuration changes needed for deployment.

## 1. Configure Insecure Docker Repository on Server

To enable Docker to communicate with an insecure repository (HTTP-based), add the following configuration to the server. Create a file `/etc/docker/daemon.json` with the following content:

```json
{
  "insecure-registries" : [ "{repo-address}:{repo-port}" ]
}
```

Restart Docker to apply the configuration:

```bash
sudo service docker restart
```

Verify that the insecure repository was added:

```bash
docker info
```

## 2. Log in to Docker Repository on the Server
Use docker login to authenticate with your Docker repository:

```bash
docker login {repo-address}:{repo-port}
```

## 3. Update Application's HOST Configuration
In your application's index.html file (located at src/main/resources/static/index.html), change the hardcoded HOST variable to the server's IP address (where you will deploy the application):

```bash
const HOST = "{server-ip-address}";
```

## 4. Rebuild and Push the Docker Image
Rebuild your application and the Docker image with the updated configuration. Push the image to your Docker repository:

```bash
gradle build
docker build -t {repo-name}/java-app:1.0-SNAPSHOT .
docker push {repo-name}/java-app:1.0-SNAPSHOT
```

## 5. Copy Docker Compose File to the Server
Use scp to copy the Docker Compose file (docker-compose.yaml) to your remote server:

```bash
scp -i ~/.ssh/id_rsa docker-compose.yaml {server-user}@{server-ip}:/home/{server-user}
```

## 6. Set Environment Variables on the Server
On the remote server, set all the required environment variables as you did in Exercise 6.

## 7. Run Docker Compose on the Server
Run Docker Compose to start all three containers on the server:

```bash
docker-compose -f docker-compose.yaml up
```

## 8. Open Port 8080 on the Server
Make sure to open port 8080 on the server to access your Java application.

By following these steps, your Java application will be running on the remote server using Docker Compose, and it will be accessible via the server's IP address.

# Contributions
Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License
Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

# Author
Adetayo Michael Ibijemilusi
