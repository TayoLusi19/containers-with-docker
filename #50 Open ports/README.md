# Exercise 8: Open Ports

In this exercise, you will open the necessary port on your server's firewall to allow access to your application from the browser.

## 1. Open the Necessary Port on the Server Firewall

Use the appropriate command to open the required port on your server's firewall. Replace `{port-number}` with the actual port number your application is running on (in this case, it's port 8080):

```bash
sudo ufw allow {port-number}/tcp
```

For example, to open port 8080:

```bash
sudo ufw allow 8080/tcp
```

## 2. Test Access from the Browser
After opening the port, you can now test access to your application from the browser. Open a web browser and enter the following URL:

```bash
http://{server-ip-address}:{port-number}
```

Replace {server-ip-address} with your server's IP address and {port-number} with the port number where your application is running (e.g., 8080).

If the configuration is correct and the port is open, you should be able to access your application from the browser.

By completing this exercise, you ensure that your application is accessible from the browser by opening the necessary port on your server's firewall.

# Contributions
Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License
Refer to the [LICENSE.md](https://gitlab.com/TayoLusi19/containers-with-docker/-/blob/main/LICENSE.md) file for licensing information.

# Author
Adetayo Michael Ibijemilusi
